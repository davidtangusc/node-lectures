var express = require('express');
var cors = require('cors');

var port = process.env.PORT || 3000;
var app = express();
var Genre = require('./models/Genre');
var Song = require('./models/Song');

// app.use(cors());
app.use(function(request, response, next) {
  response.header('Access-Control-Allow-Origin', '*');
  next();
});

app.get('/api/genres', function(request, response) {
  Genre.findAll().then((genres) => {
    response.json(genres);
  });
});

app.get('/api/songs', function(request, response) {
  Song.findAll().then((songs) => {
    response.json(songs);
  });
});

app.get('/api/songs/:id', function(request, response) {
  Song.findById(request.params.id).then((song) => {
    if (song) {
      return response.json(song);
    }

    return response.status(404).json({
      error: 'Song not found'
    });
  });
});

app.listen(port, function() {
	console.log('Listening on port ' + port)
});
